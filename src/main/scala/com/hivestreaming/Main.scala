package com.hivestreaming

import scala.concurrent.duration._

object Main extends App {

  val settings = Configuration.readSettings

  val collector = new MetricsCollector
  val sender = new MetricsSender
  val scheduler = new Scheduler(settings, collector, sender)

  scheduler.scheduleSend(0.seconds, settings.sendInterval)

}
