package com.hivestreaming

import java.lang.management.ManagementFactory
import java.time.ZonedDateTime

import com.hivestreaming.models.Metrics
import com.sun.management.OperatingSystemMXBean


object MetricsCollector {

  case class Observation(processCpuTime: Long, jvmUptime: Long)

}

class MetricsCollector {

  import MetricsCollector._

  private var previousObservation: Observation = Observation(0, 0)

  private def readCpuTime(): Observation = {
    val usage = ManagementFactory.getOperatingSystemMXBean.asInstanceOf[OperatingSystemMXBean].getProcessCpuTime

    val time = ManagementFactory.getRuntimeMXBean.getUptime * 1000000
    Observation(usage, time)
  }

  private def calculateCpuUsage(obs1: Observation, obs2: Observation): Double = {
    (obs2.processCpuTime - obs1.processCpuTime) / (obs2.jvmUptime - obs1.jvmUptime).toDouble * 100
  }

  def collect(): Metrics = {
    val currentObservation = readCpuTime()
    val cpuUsage = calculateCpuUsage(previousObservation, currentObservation)
    previousObservation = currentObservation

    Metrics(cpuUsage, ZonedDateTime.now())
  }
}
