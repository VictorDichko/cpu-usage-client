package com.hivestreaming

import java.util.concurrent.TimeUnit

import com.typesafe.config.ConfigFactory

import scala.concurrent.duration.FiniteDuration

object Configuration {

  case class Settings(clientId: String, targetUrl: String, sendInterval: FiniteDuration)

  lazy val readSettings: Settings = {
    val config = ConfigFactory.defaultApplication().resolve()
    val clientId = config.getString("clientId")
    val targetUrl = config.getString("targetUrl")
    val sendInterval = FiniteDuration(config.getDuration("sendInterval", TimeUnit.SECONDS), TimeUnit.SECONDS)

    Settings(clientId, targetUrl, sendInterval)
  }
}
