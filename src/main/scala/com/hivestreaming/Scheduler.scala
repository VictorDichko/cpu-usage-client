package com.hivestreaming

import akka.actor
import akka.actor.ActorSystem
import com.hivestreaming.Configuration.Settings

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration.FiniteDuration

class Scheduler(settings: Settings, collector: MetricsCollector, sender: MetricsSender) {

  val actorSystem = ActorSystem()
  val scheduler: actor.Scheduler = actorSystem.scheduler
  implicit val executor: ExecutionContextExecutor = actorSystem.dispatcher


  def collectAndSend(): Unit = {
    val metrics = collector.collect()
    sender.sendMetrics(settings.clientId, settings.targetUrl, metrics).unsafeRunSync()
  }

  def scheduleSend(initialDelay: FiniteDuration, interval: FiniteDuration): Unit = {

    scheduler.schedule(initialDelay, interval)(collectAndSend())
  }
}
