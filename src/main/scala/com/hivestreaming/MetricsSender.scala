package com.hivestreaming

import cats.effect.{ContextShift, IO}
import com.hivestreaming.models.{Formats, Metrics}
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s.Method._
import org.http4s.Uri
import org.http4s.circe._
import org.http4s.client.blaze._
import org.http4s.client.dsl.io._
import org.log4s.{Logger, getLogger}

import scala.concurrent.ExecutionContext

class MetricsSender extends Formats {

  val logger: Logger = getLogger

  implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global
  implicit val cs: ContextShift[IO] = IO.contextShift(ec)

  def sendMetrics(clientId: String, targetUrl: String, metrics: Metrics): IO[String] = {
    Uri.fromString(targetUrl) match {
      case Right(uri) =>
        val postRequest = POST(uri / clientId, metrics.asJson)

        logger.info(s"Sending  request: ${metrics.asJson.noSpaces}")
        BlazeClientBuilder[IO](ec).resource.use { client =>
          client.fetch(postRequest) { result =>
            logger.info(s"Got response: $result")
            result.as[String]
          }
        }
      case Left(err) =>
        val message = "Invalid URI: " + err
        println(message)
        IO.raiseError(new IllegalArgumentException(message))
    }
  }

}
