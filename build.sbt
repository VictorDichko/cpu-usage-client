name := "cpu-usage-client"

version := "0.1"

scalaVersion := "2.12.6"

val http4sVersion = "0.19.0-SNAPSHOT"
val circeVersion = "0.10.0-M2"

resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies ++= Seq(
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.akka" %% "akka-actor" % "2.5.16",
  "org.http4s" %% "http4s-blaze-client" % http4sVersion,
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-circe" % http4sVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-literal" % circeVersion,
  "com.typesafe" % "config" % "1.3.2"
)

enablePlugins(JavaAppPackaging)