# CPU usage client

The app measures CPU usage at client and report that to a server.

## Prerequisites

Installed [sbt](https://www.scala-sbt.org/download.html)  

## Running the application

To configure the endpoint where the CPU usage will be send:  
(where client id will be appended at the end of the path)    
```shell
export SERVER_URL="http://localhost:8080/metrics"
```
To configure the interval at which CPU usage will be send:  
```shell
export SEND_INTERVAL="1 second"
```
To run the application:  
```sbtshell
sbt "runMain com.hivestreaming.Main"
```

The logs are written in `application.log` file

## Build

Application could be build using [SBT Native Packager](https://www.scala-sbt.org/sbt-native-packager/)

For example:
```sbtshell
sbt universal:packageBin
```

To run application from build:

Extract zip
```
unzip target/universal/cpu-usage-client-0.1.zip -d target/universal/
```

Run it
```
./target/universal/cpu-usage-client-0.1/bin/cpu-usage-client
```


## Possible improvements

- Add authentication / encryption between client and server
- Generate client id once on application start
- Improve measuring overall CPU usage
- Show detailed messages about application status for user
- Improve on error handling
- Improve test coverage